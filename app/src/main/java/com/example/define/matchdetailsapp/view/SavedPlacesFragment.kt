package com.example.define.matchdetailsapp.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.define.matchdetailsapp.Base.BaseFragment
import com.example.define.matchdetailsapp.R
import com.example.define.matchdetailsapp.model.MyResponse
import com.example.define.matchdetailsapp.model.OnItemClickListner
import com.example.define.matchdetailsapp.viewmodel.PlaceViewModel

class SavedPlacesFragment : BaseFragment(){
    private lateinit var rvPlacesType: RecyclerView
    private lateinit var progressBar: ProgressBar

    private lateinit var placeAdapter: PlaceAdapter
    private lateinit var llm: LinearLayoutManager
    private var fragment: BaseFragment? = null
    private  var id : String = "id"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_places_type, container, false)

        rvPlacesType = view.findViewById(R.id.rvPlacesType)
        progressBar = view.findViewById(R.id.progressBar)
        initViews()
        initProgressBar()
        getPlacesData()

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    private fun initViews() {
        llm = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvPlacesType.layoutManager = llm

        if (context != null) {
            placeAdapter = PlaceAdapter(context!!, object : OnItemClickListner {
                override fun onClick(str: String) {

                }
            })
        }

    }

    private fun initProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun getPlacesData() {

//        val model = ViewModelProviders.of(this).get(PlaceViewModel::class.java!!)
//
//        model.getPlacesDetailsList(context,"40.7484,-73.9857","NPKYZ3WZ1VYMNAZ2FLX1WLECAWSMUVOQZOIDBN53F3LVZBPQ","20180616").observe(this, Observer<MyResponse>
//        { matchResponse ->
//            if (progressBar != null)
//                progressBar.visibility = View.GONE
//            placeAdapter.setList(matchResponse)
//            Log.i("list", "onChanged: places list" + matchResponse!!)
//            rvPlacesType.setAdapter(placeAdapter)
//
//        })
    }

    override fun lazyLoad() {

    }

    override fun isSupportRxBus(): Boolean {
       return false
    }

}
