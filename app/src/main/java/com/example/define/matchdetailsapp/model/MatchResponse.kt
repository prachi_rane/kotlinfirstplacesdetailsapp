package com.example.define.matchdetailsapp.model

import com.google.gson.annotations.SerializedName

data class MatchResponse (
@SerializedName("venues") val venues : List<Venues>,
	val confident : Boolean
)