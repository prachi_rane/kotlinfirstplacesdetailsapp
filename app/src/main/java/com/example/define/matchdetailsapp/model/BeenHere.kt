package com.example.define.matchdetailsapp.model

data class BeenHere  (

	val lastCheckinExpiredAt : Int
)