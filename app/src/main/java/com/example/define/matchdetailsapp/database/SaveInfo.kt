package com.example.define.matchdetailsapp.database

class SaveInfo {
    var userId = ""
    var userName = ""
    var userContact = ""
    var userAddress = ""
    var userKey = ""

    fun setKeyId(string: String) {
        userKey = string
    }
}
