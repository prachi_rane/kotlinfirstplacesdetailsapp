package com.example.define.matchdetailsapp.model

data class Meta (

	val code : Int,
	val requestId : String
)