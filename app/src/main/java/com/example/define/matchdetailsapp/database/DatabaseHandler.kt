package com.example.define.matchdetailsapp.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.define.matchdetailsapp.model.Venues

class DatabaseHandler(context: Context?) :
        SQLiteOpenHelper(context, DB_NAME, null, DB_VERSIOM) {

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME " +
                "($ID Integer PRIMARY KEY, $NAME TEXT, $CONTACT TEXT, $ADDRESS TEXT)"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // Called when the database needs to be upgraded
    }

    //Inserting (Creating) data
    fun addPlacesData(venues: Venues): Boolean {
        //Create and/or open a database that will be used for reading and writing.
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(ID , venues.id)
        values.put(NAME, venues.name)
        values.put(CONTACT, venues.contact.phone)
        values.put(ADDRESS, venues.location.address)
        val _success = db.insert(TABLE_NAME, null, values)
        db.close()
        Log.v("InsertedID", "$_success")
        return (Integer.parseInt("$_success") != -1)
    }

    //get all places
    fun getAllPlaces(): String {
        var allPlaces: String = "";
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    var id = cursor.getString(cursor.getColumnIndex(ID))
                    var name = cursor.getString(cursor.getColumnIndex(NAME))
                    var contact = cursor.getString(cursor.getColumnIndex(CONTACT))
                    var address = cursor.getString(cursor.getColumnIndex(ADDRESS))

                    allPlaces = "$allPlaces\n$id $name $contact$address"
                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allPlaces
    }

    companion object {
        private val DB_NAME = "PlacesDB"
        private val DB_VERSIOM = 1;
        private val TABLE_NAME = "places"

        private val ID = "id"
        private val NAME = "FirstName"
        private val CONTACT = "Contact"
        private val ADDRESS = "Address"
    }
}