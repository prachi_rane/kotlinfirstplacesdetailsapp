package com.example.define.matchdetailsapp.api

import com.example.define.matchdetailsapp.model.MatchResponse
import com.example.define.matchdetailsapp.model.MyResponse

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIInterface {
    @GET("https://api.foursquare.com/v2/venues/search")
    fun doGetPlacesDetails(@Query("ll") ll: String, @Query("oauth_token") oauth_token: String, @Query("v") v: String): Call<MyResponse>
}



