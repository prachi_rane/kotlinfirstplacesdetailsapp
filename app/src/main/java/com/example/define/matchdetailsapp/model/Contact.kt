package com.example.define.matchdetailsapp.model

data class Contact (

	val phone : String = "",
	val formattedPhone : String
)