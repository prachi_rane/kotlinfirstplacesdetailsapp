package com.example.define.matchdetailsapp.model

data class Json4Kotlin_Base (

	val meta : Meta,
	val notifications : List<Notifications>,
	val matchResponse : MatchResponse
)