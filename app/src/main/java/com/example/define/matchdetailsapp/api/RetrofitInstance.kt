package com.example.define.matchdetailsapp.api

import com.example.define.matchdetailsapp.utils.Constants

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private var mInstance: RetrofitInstance? = null
    private var apiInterface: APIInterface? = null

    val instance: RetrofitInstance
        get() {
            if (mInstance == null) {
                mInstance = RetrofitInstance
            }
            return mInstance as RetrofitInstance
        }

    fun getApiInterface(): APIInterface? {
        if (apiInterface == null) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)  // <-- this is the important line!
            apiInterface = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(APIInterface::class.java)
        }
        return apiInterface
    }
}
