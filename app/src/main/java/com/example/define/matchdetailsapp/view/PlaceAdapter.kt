package com.example.define.matchdetailsapp.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.define.matchdetailsapp.R
import com.example.define.matchdetailsapp.model.MatchResponse
import com.example.define.matchdetailsapp.model.MyResponse
import com.example.define.matchdetailsapp.model.OnItemClickListner
import kotlinx.android.synthetic.main.nav_header_main.view.*

class PlaceAdapter(private val context: Context, private val onItemClickListner: OnItemClickListner) : RecyclerView.Adapter<PlaceAdapter.ViewHolder>() {
    private var matchResponse: MyResponse? = null
    var metrices: DisplayMetrics

    init {
        metrices = context.resources.displayMetrics
    }

    fun setList(matchResponse: MyResponse?) {
        this.matchResponse = matchResponse
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.rv_item_places, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvPlaceId.text = matchResponse!!.matchResponse.venues[position].id
        holder.tvPlaceName.text = matchResponse!!.matchResponse.venues[position].name
        holder.tvPlaceContact.text = matchResponse!!.matchResponse.venues[position].contact.phone
        holder.tvPlaceAddress.text = matchResponse!!.matchResponse.venues[position].location.address

        holder.rlParent.setOnClickListener { onItemClickListner.onClick((matchResponse!!.matchResponse.venues[position].id)) }
        holder.ivStar.setOnClickListener {  holder.ivStar.setImageResource(holder.imgResId) }
    }

    override fun getItemCount(): Int {
        if(matchResponse == null)
            return 0
        else
        return matchResponse!!.matchResponse.venues.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvPlaceId: TextView
        internal var tvPlaceName: TextView
        internal var tvPlaceContact: TextView
        internal var tvPlaceAddress: TextView
        internal var rlParent: RelativeLayout
        internal var ivStar : ImageView

        val imgResId = R.drawable.ic_star_filled
        var resId = imgResId

        init {
            tvPlaceId = itemView.findViewById(R.id.tvPlaceId)
            tvPlaceName = itemView.findViewById(R.id.tvPlaceName)
            tvPlaceContact = itemView.findViewById(R.id.tvPlaceContact)
            tvPlaceAddress = itemView.findViewById(R.id.tvPlaceAddress)
            rlParent = itemView.findViewById(R.id.rlParent)
            ivStar = itemView.findViewById(R.id.ivStar)

        }

    }
}