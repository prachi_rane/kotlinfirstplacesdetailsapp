package com.example.define.matchdetailsapp.model

data class HereNow (

	val count : Int,
	val summary : String,
	val groups : List<String>
)