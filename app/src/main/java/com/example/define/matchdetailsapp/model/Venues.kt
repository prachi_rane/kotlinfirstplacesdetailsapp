package com.example.define.matchdetailsapp.model

data class Venues (

	val id : String = "",
	val name : String = "",
	val contact : Contact,
	val location : Location,
	val categories : List<Categories>,
	val verified : Boolean,
	val stats : Stats,
	val beenHere : BeenHere,
	val specials : Specials,
	val venuePage : VenuePage,
	//val hereNow : HereNow,
	val referralId : String,
	//val venueChains : List<String>,
	val hasPerk : Boolean
)