package com.example.define.matchdetailsapp.model

data class Stats (

	val tipCount : Int,
	val usersCount : Int,
	val checkinsCount : Int
)