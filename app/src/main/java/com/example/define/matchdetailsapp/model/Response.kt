package com.example.define.matchdetailsapp.model

import com.google.gson.annotations.SerializedName

data class MyResponse(@SerializedName("response") val matchResponse : MatchResponse)

//data class Response(@SerializedName("venues")val venues : ArrayList<Venues>)
//@Entity(tableName = "Venues")
//data class Venues(
//        @SerializedName("id") @PrimaryKey val id: String, @SerializedName("name") @ColumnInfo(name = "name") val name: String,
//        @ColumnInfo(name = "isSaved") var isSaved: Boolean? = false
//)