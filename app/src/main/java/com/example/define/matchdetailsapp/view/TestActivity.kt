package com.example.define.matchdetailsapp.view

import android.arch.lifecycle.Lifecycle
import android.os.Bundle
import android.support.v4.app.Fragment

import com.example.define.matchdetailsapp.Base.BaseActivity
import com.example.define.matchdetailsapp.R

class TestActivity : BaseActivity() {
    private var fragment: Fragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragment = AllPlacesFragment()
        replaceFragment(fragment, R.id.frame, true)
    }

    override fun isSupportRxBus(): Boolean {
        return false
    }

    override fun onEvent(event: Lifecycle.Event) {

    }


}
