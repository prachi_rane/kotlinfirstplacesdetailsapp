package com.example.define.matchdetailsapp.model

data class LabeledLatLngs (

	val label : String,
	val lat : Double,
	val lng : Double
)