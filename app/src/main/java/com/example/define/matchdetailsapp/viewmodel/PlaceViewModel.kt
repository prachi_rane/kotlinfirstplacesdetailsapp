package com.example.define.matchdetailsapp.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.widget.Toast

import com.example.define.matchdetailsapp.R
import com.example.define.matchdetailsapp.api.RetrofitInstance
import com.example.define.matchdetailsapp.model.MatchResponse
import com.example.define.matchdetailsapp.model.MyResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlaceViewModel : ViewModel() {

    private var placesDetailsList: MutableLiveData<MyResponse>? = null

    fun getPlacesDetailsList(context: Context?, ll: String, oauth_token: String, v: String): LiveData<MyResponse> {
        if (placesDetailsList == null) {
            placesDetailsList = MutableLiveData()
            loadPackageTypeList(context, ll, oauth_token, v)
        }
        return placesDetailsList as MutableLiveData<MyResponse>
    }

    private fun loadPackageTypeList(context: Context?, ll: String, oauth_token: String, v: String) {
        RetrofitInstance.getApiInterface()!!.doGetPlacesDetails(ll, oauth_token, v).enqueue(object : Callback<MyResponse> {
            override fun onResponse(call: Call<MyResponse>, response: Response<MyResponse>) {
                if (response.body() != null) {

                    placesDetailsList!!.value = response.body()
                    Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(context, R.string.fail, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<MyResponse>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }
}
