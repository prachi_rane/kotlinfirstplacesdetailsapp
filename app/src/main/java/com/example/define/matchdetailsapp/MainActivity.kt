package com.example.define.matchdetailsapp

import android.arch.lifecycle.Lifecycle
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.Toast
import com.example.define.matchdetailsapp.Base.BaseActivity
import com.example.define.matchdetailsapp.view.AllPlacesFragment
import com.example.define.matchdetailsapp.view.SavedPlacesFragment

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var drawerLayout : DrawerLayout
    private lateinit var frame : FrameLayout
    private var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        frame = findViewById(R.id.frame)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        fragment = AllPlacesFragment()
        replaceFragment(fragment, R.id.frame, false)

    }

    private fun initViews(){

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_all_places -> {
                fragment = AllPlacesFragment()
                replaceFragment(fragment, R.id.frame, true)

                Toast.makeText(this,"All Places",Toast.LENGTH_SHORT).show()
                // Handle the camera action
            }
            R.id.nav_saved_places -> {
                fragment = SavedPlacesFragment()
                replaceFragment(fragment,R.id.frame,true)
                Toast.makeText(this, "Saved Places",Toast.LENGTH_SHORT).show()
            }

        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun isSupportRxBus(): Boolean {
        return false
    }

    override fun onEvent(event: Lifecycle.Event) {

    }
}
