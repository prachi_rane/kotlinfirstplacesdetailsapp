package com.example.define.matchdetailsapp.Base;

import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;


public abstract class BaseActivity extends AppCompatActivity  {

    protected Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        if (isSupportRxBus()) {
            subscribeRxBus();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

       // EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
       // EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
      //  MyApplication.getInstance().setConnectivityListener(this);
    }

    protected abstract boolean isSupportRxBus();


    private void subscribeRxBus() {
//        unSubscribeRxBus();
//        mRxBusSubscription = RxBus.getDefault().toObservable(RxEvent.class)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new RxBusSubscriber<RxEvent>() {
//
//                    @Override
//                    protected void onEvent(RxEvent event) {
//                        onRxEvent(event);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        super.onError(e);
//                        subscribeRxBus();
//                    }
//                });
    }

    private void unSubscribeRxBus() {
//        if (mRxBusSubscription != null && !mRxBusSubscription.isUnsubscribed()) {
//            mRxBusSubscription.unsubscribe();
//        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unSubscribeRxBus();
    }

    /**
     * add fragment
     *  @param fragment
     * @param frameId
     * @param backstack
     */
    protected void addFragment(Fragment fragment, @IdRes int frameId, boolean backstack) {
        // AppConstants.checkNotNull(fragment);
        if (getSupportFragmentManager() ==null)return;
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(frameId, fragment, fragment.getClass().getSimpleName());
        if (backstack)
            fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commitAllowingStateLoss();
    }

    /**
     * replace fragment
     *
     * @param fragment
     * @param frameId
     */
    protected void replaceFragment(Fragment fragment, @IdRes int frameId, boolean backstack) {
        //AppConstants.checkNotNull(fragment);
        if (getSupportFragmentManager() ==null)return;
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frameId, fragment, fragment.getClass().getSimpleName());
        if (backstack)
            fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commitAllowingStateLoss();
    }


    protected void hideFragment(Fragment fragment) {
        //AppConstants.checkNotNull(fragment);
        getSupportFragmentManager().beginTransaction()
                .hide(fragment)
                .commitAllowingStateLoss();
    }

    /**
     * show fragment
     *
     * @param fragment
     */
    protected void showFragment(Fragment fragment) {
        //AppConstants.checkNotNull(fragment);
        getSupportFragmentManager().beginTransaction()
                .show(fragment)
                .commitAllowingStateLoss();
    }

    /**
     * remove fragment
     *
     * @param fragment
     */
    protected void removeFragment(Fragment fragment) {
        // AppConstants.checkNotNull(fragment);
        getSupportFragmentManager().beginTransaction()
                .remove(fragment)
                .commitAllowingStateLoss();
    }

    /**
     * pop fragment from the top of stack
     */
    protected void popFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public AlertDialog.Builder buildDialog(Context context)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Network Error");
        builder.setMessage("Oops, something went wrong! Please check your internet connection....");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        return builder;
    }

    public abstract void onEvent(Lifecycle.Event event);




}
