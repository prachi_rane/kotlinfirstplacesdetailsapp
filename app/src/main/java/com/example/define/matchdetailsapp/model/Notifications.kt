package com.example.define.matchdetailsapp.model

data class Notifications (

	val type : String,
	val item : Item
)