package com.example.define.matchdetailsapp.Base;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    protected BaseActivity mActivity;
    protected boolean isVisble = true;
    protected boolean isFirst = true;

    private String tabTitle;

    public String getTabTitle() {
        return tabTitle;
    }

    public void setTabTitle(String tabTitle) {
        this.tabTitle = tabTitle;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisble = true;
            onVisible();
        } else {
            isVisble = false;
            onInvisible();
        }
    }

    protected void onVisible() {
        if (isFirst) {
            isFirst = false;
            if (isSupportRxBus()) {
                subscribeRxBus();
            }
        }
        lazyLoad();
    }

    protected void onInvisible() {
    }

    protected abstract void lazyLoad();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = (BaseActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            onVisible();
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    protected abstract boolean isSupportRxBus();


    private void subscribeRxBus() {
        unSubscribeRxBus();
//        mRxBusSubscription = RxBus.getDefault().toObservable(RxEvent.class)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new RxBusSubscriber<RxEvent>() {
//                    @Override
//                    protected void onEvent(RxEvent event) {
//                        onRxEvent(event);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        super.onError(e);
//                        subscribeRxBus();
//                    }
//                });
    }

    private void unSubscribeRxBus() {
//        if (mRxBusSubscription != null && !mRxBusSubscription.isUnsubscribed()) {
//            mRxBusSubscription.unsubscribe();
//        }
    }

    /**
     * get holder`s activity
     *
     * @return BaseActivity
     */
    protected BaseActivity getHoldingActivity() {
        return mActivity;
    }


    /**
     * add fragment
     *
     * @param fragment
     * @param frameId
     */
    public void addFragment(BaseFragment fragment, @IdRes int frameId,boolean backstack) {
        //AppConstants.checkNotNull(fragment);
        getHoldingActivity().addFragment(fragment, frameId,backstack );

    }


    /**
     * replace fragment
     *
     * @param fragment
     * @param frameId
     */
    public void replaceFragment(BaseFragment fragment, @IdRes int frameId,boolean backstack) {
        // AppConstants.checkNotNull(fragment);
        getHoldingActivity().replaceFragment(fragment, frameId,backstack);
    }


    /**
     * hide fragment
     *
     * @param fragment
     */
    protected void hideFragment(BaseFragment fragment) {
        //AppConstants.checkNotNull(fragment);
        getHoldingActivity().hideFragment(fragment);
    }


    /**
     * show fragment
     *
     * @param fragment
     */
    protected void showFragment(BaseFragment fragment) {
        // AppConstants.checkNotNull(fragment);
        getHoldingActivity().showFragment(fragment);
    }


    /**
     * remove Fragment
     *
     * @param fragment
     */
    protected void removeFragment(BaseFragment fragment) {
        // AppConstants.checkNotNull(fragment);
        getHoldingActivity().removeFragment(fragment);

    }


    /**
     * pop fragment from the top of stack
     */
    protected void popFragment() {
        getHoldingActivity().popFragment();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}

