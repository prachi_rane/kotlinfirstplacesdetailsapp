package com.example.define.matchdetailsapp.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(private val m_context: Context) : SQLiteOpenHelper(m_context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        try {
            db.execSQL(USERINFO)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    companion object {

        private val DATABASE_NAME = "PLACES"
        private val DATABASE_VERSION = 1
        private val KEY_ID = "id"


        private val USERINFO = ("CREATE TABLE IF NOT EXISTS "
                + TableSave.TABLE_USER + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TableSave.KEYUSERID + " TEXT,"
                + TableSave.KEYUSERNAME + " TEXT" + ")")
    }

}
