package com.example.define.matchdetailsapp.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

import java.util.ArrayList

class TableSave(private val mcontext: Context) {
    private val mDatabaseHelper: DatabaseHelper
    private var db: SQLiteDatabase? = null


    val allRecord: ArrayList<SaveInfo>
        get() {
            val postList = ArrayList<SaveInfo>()
            try {
                val selectQuery = "SELECT * FROM $TABLE_USER"
                val db = mDatabaseHelper.writableDatabase
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {

                        val valueCategory = SaveInfo()
                        valueCategory.userId = cursor.getString(cursor.getColumnIndex(KEYUSERID))
                        valueCategory.userName = cursor.getString(cursor.getColumnIndex(KEYUSERNAME))
                        valueCategory.userContact = cursor.getString(cursor.getColumnIndex(KEYUSERCONTACT))
                        valueCategory.userAddress = cursor.getString(cursor.getColumnIndex(KEYUSERADDRESS))
                        valueCategory.setKeyId(cursor.getString(cursor.getColumnIndex(KEY_ID)))
                        postList.add(valueCategory)

                    } while (cursor.moveToNext())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return postList
        }


    init {
        mDatabaseHelper = DatabaseHelper(mcontext)
    }


    fun insertInfo(mUserId: String, mUserName: String, mUserContact: String, mUserAddress: String) {

        try {
            db = mDatabaseHelper.writableDatabase
            val contentValues = ContentValues()
            contentValues.put(KEYUSERID, mUserId)
            contentValues.put(KEYUSERNAME, mUserName)
            contentValues.put(KEYUSERCONTACT, mUserContact)
            contentValues.put(KEYUSERADDRESS, mUserAddress)
            val status = db!!.insert(TABLE_USER, null, contentValues)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun checkAlreadyExist(mUserId: String): Boolean {
        var ISPatitentAvailable = false
        try {
            val db = mDatabaseHelper.readableDatabase
            val selectQuery = "SELECT * FROM  $TABLE_USER WHERE $KEYUSERID='$mUserId'"
            val c = db.rawQuery(selectQuery, null)
            if (c.count > 0) {
                c.moveToFirst()
                ISPatitentAvailable = true
            } else {
                ISPatitentAvailable = false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ISPatitentAvailable
    }


    fun favoriteDelete(id: String) {
        db = mDatabaseHelper.writableDatabase
        try {
            db!!.delete(TABLE_USER, "KEYUSERID = ?", arrayOf(id))
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            db!!.close()
        }
    }

    companion object {
        val KEY_ID = "id"
        val KEYUSERID = "KEYUSERID"
        val KEYUSERNAME = "KEYUSERNAME"
        val KEYUSERCONTACT = "KEYUSERCONTACT"
        val KEYUSERADDRESS = "KEYUSERADDRESS"
        val TABLE_USER = "TABLEUSER"
    }

}
