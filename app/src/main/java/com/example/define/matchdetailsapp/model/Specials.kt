package com.example.define.matchdetailsapp.model

data class Specials (

	val count : Int,
	val items : List<String>
)